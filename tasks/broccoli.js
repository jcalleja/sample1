module.exports = function() {

    return {
        dev: {
            dest: "broccoli-build"
        },
        dist: {
            dest: "dist"
        }
    }

};
