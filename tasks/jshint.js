module.exports = function() {

    return {
        files: [ 'Brocfile.js', 'Gruntfile.js', 'app/js/**/*.js' ],
        options: {
            jshintrc: ".jshintrc"
        }
    }

};
