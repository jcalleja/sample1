var BuildUtils = require("build-utils"),
    butils = new BuildUtils();

module.exports = function (opts) {

    var jsDeps = [
        "jquery/jquery.js",
        "handlebars/handlebars.js",
        "ember/ember.js",
        "ember-data/ember-data.js",
        "bootstrap/dist/js/bootstrap.js"
    ];
    var cssDeps = [
        "bootstrap/dist/css/bootstrap.css",
        "bootstrap/dist/css/bootstrap-theme.css"
    ];
    var fontDeps = [
        "bootstrap/dist/fonts/*"
    ];

    var jsMinDeps = butils.toMin(jsDeps),
        cssMinDeps = butils.toMin(cssDeps);

    var flatten = true;

    return {
        deps: {
            files: [
                // flatten: true if you don't want nesting like lib/jquery, lib/ember and just
                // put all *.js in lib
                { expand: true, flatten: flatten, cwd: 'bower_components', src: jsDeps, dest: 'lib/js', filter: 'isFile' },
                { expand: true, flatten: flatten, cwd: 'bower_components', src: cssDeps, dest: 'lib/css', filter: 'isFile' },
                { expand: true, flatten: flatten, cwd: 'bower_components', src: fontDeps, dest: 'lib/fonts', filter: 'isFile' },

                { expand: true, flatten: flatten, cwd: 'bower_components', src: jsMinDeps, dest: 'libmin/js', filter: 'isFile' },
                { expand: true, flatten: flatten, cwd: 'bower_components', src: cssMinDeps, dest: 'libmin/css', filter: 'isFile' },
                { expand: true, flatten: flatten, cwd: 'bower_components', src: fontDeps, dest: 'libmin/fonts', filter: 'isFile' }
            ]
        }
    }

};
