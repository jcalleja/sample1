// example usage: rm -rf ./build && broccoli build build
// Will build using this file and output result in `build` dir (second arg)

//var compileSass = require('broccoli-sass'),

var concatenate = require('broccoli-concat'),
    mergeTrees  = require('broccoli-merge-trees'),
    pickFiles   = require('broccoli-static-compiler'),
    uglifyJs    = require('broccoli-uglify-js'),
    templateCompiler = require('broccoli-ember-hbs-template-compiler'),
    compileSass = require('broccoli-sass'),
    app = 'app';

var appHtml = pickFiles(app, {
    srcDir  : '/',
    files   : ['index.html'],
    destDir : '/'
});

var appJs = pickFiles(app, {
    srcDir  : '/',
    files: [ '**/*.js' ],
    destDir : '/js'
});

var templates = pickFiles(app, {
    srcDir: '/templates',
    destDir: '/templates'
});
templates = templateCompiler(templates);

appJs = concatenate(mergeTrees([ appJs, templates ]), {
    inputFiles : ['**/*.js'],
    outputFile : '/js/app.js'
});

if (process.env.BROCCOLI_ENV === 'production') {
    appJs = uglifyJs(appJs, {
        compress: true
    });
}

var lib = 'lib';
if (process.env.BROCCOLI_ENV === 'production') {
    lib = 'lib/min';
}
var libJs = pickFiles(lib, {
    srcDir: '/js',
    destDir: 'js/lib'
});
var libCss = pickFiles(lib, {
    srcDir: '/css',
    destDir: 'css/lib'
});
var libFonts = pickFiles(lib, {
    srcDir: '/fonts',
    destDir: 'fonts/lib'
});

lib = mergeTrees([ libJs, libCss, libFonts ]);
/**
 * compile all of the SASS in the project /resources folder into
 * a single app.css file in the build production/resources folder
 */
appCss = compileSass(
    ['resources/sass'],
    '/app.scss',
    'production/resources/app.css'
);

module.exports = mergeTrees([appHtml, appJs, lib]);
