var extend = require('extend');

module.exports = function(grunt) {

    grunt.initConfig( gruntInitConfig() );

    function config(name, customOpts) {
        var opts = {
            'grunt': grunt
        };
        return require('./tasks/' + name)(extend(opts, customOpts));
    }

    function gruntInitConfig() {
        var pkg = grunt.file.readJSON('package.json');

        return {
            pkg: pkg,
            clean: config('clean'),
            copy: config('copy'),
            jshint: config('jshint'),
            broccoli: config('broccoli')
        };
    }

    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-broccoli');

    grunt.registerTask('default', [ 'buildDevelopment' ]);

    grunt.registerTask('buildDevelopment', [
        'clean:deps',
        'copy:deps',
        'jshint',
        'broccoli:dev:build'
    ]);

//    grunt broccoli:{targetName}:watch
//    grunt broccoli:{targetName}:serve

    // grunt.registerTask('cleanup', 'Deletes any generated tmp files', function() {
    // 	var tmpJsFiles = "./public/js/*.tmp.js";
    // 	shell.echo("rm " + "%s", tmpJsFiles);
    // 	shell.rm(tmpJsFiles);
    // });

};
